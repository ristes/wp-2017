import {Video} from "../../model/Video";


import {Observable} from 'rxjs/Observable';
import {Category} from "../../model/Category";

export abstract class VideoServiceInterface {

  abstract load(): Observable<Video[]>;

  abstract save(video: Video): Observable<Video>;

  abstract edit(originalVideo: Video, updatedVideo: Video): Observable<Video>;

  abstract findByTitle(videoTitle: string): Observable<Video[]>;

  abstract addTag(videoId: number, tagName: string): Observable<any>;

  abstract removeTag(videoId: number, tagName: string): Observable<any>;

  abstract loadCategories(): Observable<Category[]>;
}
