import {Pipe, PipeTransform} from '@angular/core';
import {Video} from '../../model/Video';

@Pipe({
  name: 'videoTitle'
})
export class VideoTitlePipe implements PipeTransform {

  transform(value: Video, args?: any): string {
    return value ? value.title : '';
  }

}
