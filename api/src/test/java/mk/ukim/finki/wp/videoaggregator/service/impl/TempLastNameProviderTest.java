package mk.ukim.finki.wp.videoaggregator.service.impl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TempLastNameProviderTest {

    TempLastNameProvider tempLastNameProvider;

    @Before
    public void setup() {
        tempLastNameProvider = new TempLastNameProvider();
    }

    @Test
    public void test_last_name() {
        String expected = "Stojanov";
        String actual = tempLastNameProvider.lastName("name");
        assertEquals(expected, actual);
    }
}