package mk.ukim.finki.wp.videoaggregator;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.service.impl.ICacheTestHelper;
import net.sf.ehcache.CacheManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.assertEquals;

/**
 * @author Riste Stojanov
 */

@ComponentScan("mk.ukim.finki.wp.videoaggregator.service.impl")
@ActiveProfiles({"test", "jpa"})
public class CachingTest extends BaseIntegrationTest {


  @Autowired
  ICacheTestHelper helper;

  @PersistenceContext
  EntityManager em;

  @Test
  public void testCache() {

    int size = CacheManager.ALL_CACHE_MANAGERS.get(0)
      .getCache(Category.class.getName()).getSize();
    assertEquals(0, size);

    Category test = new Category();
    test.title = "test";
    test = helper.save(test);

    em.clear();
    Category c = helper.findOne(test.id);

    c = helper.findOne(test.id);


    System.out.println("before check");
    size = CacheManager.ALL_CACHE_MANAGERS.get(0)
      .getCache(Category.class.getName()).getSize();
    assertEquals(1, size);

    helper.delete(test.id);

    size = CacheManager.ALL_CACHE_MANAGERS.get(0)
      .getCache(Category.class.getName()).getSize();
    assertEquals(0, size);
  }

}
