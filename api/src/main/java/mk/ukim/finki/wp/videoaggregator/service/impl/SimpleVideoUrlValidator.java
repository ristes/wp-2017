package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.exceptions.InvalidVideoUrl;
import mk.ukim.finki.wp.videoaggregator.service.VideoUrlValidator;
import org.springframework.stereotype.Component;

@Component
public class SimpleVideoUrlValidator implements VideoUrlValidator {
    private static final String HTTP_YOUTUBE_COM = "http://youtube.com";
    private static final String HTTPS_YOUTUBE_EMBED = "https://youtu.be";

    @Override
    public void validate(String url) {
        if (!url.startsWith(HTTP_YOUTUBE_COM) && !url.startsWith(HTTPS_YOUTUBE_EMBED)) {
            throw new InvalidVideoUrl();
        }
    }
}
