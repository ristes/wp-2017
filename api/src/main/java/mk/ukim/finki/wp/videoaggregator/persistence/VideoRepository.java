package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface VideoRepository {

  Video save(Video video);

  Optional<Video> findOne(Long id);

  Optional<Video> findOneVideoWithTags(Long id);

  void delete(Video video);

  Iterable<Video> findByCategoryTitle(String title);

  /**
   * This method is deprecated. Please replace it with findAll(Pageable).
   *
   * @return
   */
  @Deprecated
  Iterable<Video> findAll();

  Page<Video> findAll(Pageable pageable);

  Iterable<Video> findAllFetchingAllProperties();

  Iterable<Video> findByTagsName(String tagName);

  Iterable<Video> findByTitleIgnoreCaseContaining(String title);

  Page<Video> findAll(Example<Video> example, Pageable pageable);

}
