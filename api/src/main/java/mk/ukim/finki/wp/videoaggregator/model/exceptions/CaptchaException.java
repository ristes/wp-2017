package mk.ukim.finki.wp.videoaggregator.model.exceptions;

public class CaptchaException extends RuntimeException {
  public CaptchaException(String message){
    super(message);
  }
}
