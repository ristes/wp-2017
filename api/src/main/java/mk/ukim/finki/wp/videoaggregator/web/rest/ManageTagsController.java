package mk.ukim.finki.wp.videoaggregator.web.rest;

import mk.ukim.finki.wp.videoaggregator.model.Tag;
import mk.ukim.finki.wp.videoaggregator.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Riste Stojanov
 */
@RestController
@RequestMapping(value = "/api/tags", produces = MediaType.APPLICATION_JSON_VALUE)
public class ManageTagsController {

  private final TagService tagService;

  @Autowired
  public ManageTagsController(TagService tagService) {
    this.tagService = tagService;
  }

  @RequestMapping(method = RequestMethod.POST)
  public Tag createTag(@RequestBody Tag tag) {
    return tagService.save(tag);
  }

}
