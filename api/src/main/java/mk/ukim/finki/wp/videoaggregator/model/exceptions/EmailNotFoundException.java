package mk.ukim.finki.wp.videoaggregator.model.exceptions;

public class EmailNotFoundException extends RuntimeException {
  public EmailNotFoundException() {
    super("Email not found");
  }
}
