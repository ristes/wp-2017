package mk.ukim.finki.wp.videoaggregator.model.exceptions;

public class DuplicateEmailException extends RuntimeException {

  public DuplicateEmailException() {
    super("Email already exists");
  }

}
