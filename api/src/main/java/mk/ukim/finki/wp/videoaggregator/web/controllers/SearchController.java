package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.SearchService;
import mk.ukim.finki.wp.videoaggregator.service.SearchVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author Riste Stojanov
 */
@Controller
public class SearchController {

  private final SearchVideoService searchVideoService;
  private final SearchService searchService;

  @Autowired
  public SearchController(SearchVideoService searchVideoService, SearchService searchRepository) {
    this.searchVideoService = searchVideoService;
    this.searchService = searchRepository;
  }

  @GetMapping("/search")
  public String search(ModelMap model, @RequestParam String query) {
    List<Video> res = searchService.searchVideo(query);
//    model.addAttribute("prevPageRequest", buildURIFromParams(query, 0, res.size()));
//    model.addAttribute("nextPageRequest", buildURIFromParams(query, 0, res.size());
    model.addAttribute("query", query);
    model.addAttribute("videos", res);
    model.addAttribute("pageNumber", 0);
    model.addAttribute("prevPage", 0);
    model.addAttribute("nextPage", 0);
    model.addAttribute("hasNext", false);
    model.addAttribute("hasPrev", false);

    return "fragments/contents";
  }

  @GetMapping("/search-by-example")
  public String searchByExample(ModelMap modelMap, @RequestParam String query){
    ExampleMatcher exampleMatcher = ExampleMatcher
      .matching()
      .withIgnoreCase()
      .withIgnoreNullValues()
      .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

    Video example = new Video();
    example.title = query;

    Page<Video> videos = searchVideoService.findAll(Example.of(example, exampleMatcher), getPageable(1,5));

    modelMap.addAttribute("videos", videos);

    return "fragments/contents";
  }

  @GetMapping("/search-by-specification")
  public String searchBySpecification(ModelMap modelMap, @RequestParam String queryString){

    Specification<Video> specification = (root, query, cb) -> cb.like(root.get("title"),"%" + queryString + "%");

    Page<Video> videos = searchVideoService.findAll(specification, getPageable(1,5));

    modelMap.addAttribute("videos", videos);

    return "fragments/contents";
  }

  private Pageable getPageable(Integer page, Integer size){
    return new PageRequest(page, size, new Sort(Sort.Direction.DESC, LandingController.PAGE_ORDER_BY_CRITERIA));
  }
}
