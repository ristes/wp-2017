package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.SearchRepository;
import mk.ukim.finki.wp.videoaggregator.service.SearchService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Riste Stojanov
 */
@Service
public class SearchServiceImpl implements SearchService {
  private final SearchRepository repository;

  public SearchServiceImpl(SearchRepository repository) {
    this.repository = repository;

  }

  @Override
  public List<Video> searchVideo(String phrase) {
    return repository.searchPhrase(Video.class, phrase,
      "title",
      "description",
      "category.name",
      "tag.tagName");
  }
}
