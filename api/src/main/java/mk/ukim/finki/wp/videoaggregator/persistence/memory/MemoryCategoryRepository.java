package mk.ukim.finki.wp.videoaggregator.persistence.memory;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Riste Stojanov
 */
@Profile("memory")
@Repository
public class MemoryCategoryRepository implements CategoryRepository {

    private static Long idSequence = 0L;
    private Map<Long, Category> mapDb = new HashMap<>();

    @Override
    public Optional<Category> findOne(Long categoryId) {
        return Optional.ofNullable(mapDb.get(categoryId));
    }

    @Override
    public Category save(Category category) {
        idSequence++;
        Category newCategory = new Category();

        newCategory.id = idSequence;
        newCategory.title = category.title;
        return mapDb.put(newCategory.id, newCategory);
    }

  @Override
  public Iterable<Category> findAll() {
    return mapDb.values();
  }

  @Override
  public Iterable<Category> findByTitle(String title) {
    return null;
  }

  @Override
  public Iterable<Category> findByTitleLike(String title) {
    return null;
  }

  @Override
  public void delete(long id) {

  }

  @Override
  public void softDelete(long id) {

  }

  @Override
  public long count() {
    return 0;
  }
}
