package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.UserVerificationToken;

import java.util.Date;

public interface VerificationTokenRepository {

  UserVerificationToken save(UserVerificationToken token);

  UserVerificationToken findByToken(String token);

  UserVerificationToken findByUser(String user);

  void deleteByExpiryDateLessThan(Date date);

  void delete(UserVerificationToken token);
}
